package com.example.miniprojet.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.miniprojet.Accueil;
import com.example.miniprojet.R;
import com.example.miniprojet.adapters.AccueilAdapter;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AccueilFragment extends Fragment {
    RecyclerView rv;
    ArrayList<Accueil> accueils;
    AccueilAdapter accueilAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.accueil_fragment,container,false);
        accueils= new ArrayList<Accueil>();
        rv = view.findViewById(R.id.recycler);
        rv.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        accueilAdapter=new AccueilAdapter(accueils,getActivity());
        fillData();
        rv.setAdapter(accueilAdapter);
        return view;
    }


    void fillData(){
        accueils.add(new Accueil(1,"Mohamed","Jberi",R.drawable.mohamed,"26196956"));
        accueils.add(new Accueil(2,"Mahdi","Abid",R.drawable.mahdi,"27896789"));
        accueils.add(new Accueil(3,"Oussema","Jlassi",R.drawable.oussema,"25896723"));
        accueils.add(new Accueil(4,"Khalil","Hamdi",R.drawable.khalil,"21239789"));
        accueils.add(new Accueil(5,"Sabri","Gharbi",R.drawable.sabri,"26196887"));
        accueils.add(new Accueil(6,"Hassen","Dridi",R.drawable.hassen,"26196887"));

    }
}
