package com.example.miniprojet.classes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static Retrofit retrofit = null;

    static Gson gson=new GsonBuilder()
            .setLenient()
            .create();

    public static Retrofit getClient()
    {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.3:3000")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit;

    }

}