package com.example.miniprojet.dao;

import com.example.miniprojet.models.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import java.util.List;


public interface IUser {


    @GET("/login")
    Call<User> login(@Query("email")String email ) ;


    @POST("/loginn")
    Call<User> loginn(@Query("email")String email , @Query("password")String password) ;

    @GET("/usercheck")
    Call<User> executeSignup (@Query("firstName")String nom ,@Query("lastName")String prenom,
                              @Query("email")String email, @Query("password")String password
                         ,@Query("tel")String tel);
}
