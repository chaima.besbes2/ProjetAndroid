package com.example.miniprojet;

public class Accueil {
    private int id;
    private String nom;
    private String prenom;
    private String tel;
    private int image;

    public Accueil(int id) {
        this.id = id;
    }

    public Accueil(int id, String nom, String prenom, int image, String tel) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.image = image;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Accueil{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", tel=" + tel +
                ", image=" + image +
                '}';
    }
}
