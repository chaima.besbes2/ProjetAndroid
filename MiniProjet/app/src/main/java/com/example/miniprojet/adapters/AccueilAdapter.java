package com.example.miniprojet.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.miniprojet.Accueil;
import com.example.miniprojet.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class AccueilAdapter extends RecyclerView.Adapter<AccueilAdapter.VoitureHolder> {
    private Context mContext;
    private ArrayList<Accueil> acceuilArrayList;

    public AccueilAdapter(ArrayList<Accueil> acceuilArrayList, Context mContext) {
        this.mContext = mContext;
        this.acceuilArrayList = acceuilArrayList;
    }

    @NonNull
    @Override
    public VoitureHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(mContext).inflate(R.layout.single_row_accueilpage, parent, false);
        return new VoitureHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull VoitureHolder holder, int position) {
        Accueil accueil = acceuilArrayList.get(position);
        holder.txtnom.setText(accueil.getNom());
        holder.txtprenom.setText(accueil.getPrenom());
        holder.txtTel.setText(accueil.getTel());
        holder.imgAgent.setImageResource(accueil.getImage());
    }

    @Override
    public int getItemCount() {
        return acceuilArrayList.size();
    }


    public class VoitureHolder extends RecyclerView.ViewHolder {
        ImageView imgAgent;
        TextView txtnom;
        TextView txtprenom;
        TextView txtTel;


        public VoitureHolder(@NonNull View itemView) {
            super(itemView);
            imgAgent = itemView.findViewById(R.id.imgAgentAccueil);
            txtnom = itemView.findViewById(R.id.nomAgentAccueil);
            txtprenom = itemView.findViewById(R.id.prenomAgentAccueil);
            txtTel = itemView.findViewById(R.id.telAgentAccueil);
        }
    }
}
