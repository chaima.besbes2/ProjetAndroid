package com.example.miniprojet;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.miniprojet.classes.ApiClient;
import com.example.miniprojet.dao.IUser;
import com.example.miniprojet.models.User;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InscriptionActivity extends AppCompatActivity {
    EditText nom , prenom , email , password, confirmPassword,tel;
    Button btnInscription;
    TextView signup ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inscriptionpage);
        ActionBar actionBar = getSupportActionBar();
        nom=findViewById(R.id.nomInscription_text);
        prenom=findViewById(R.id.prenomInscription_text);
       email =findViewById(R.id.emailInscription_text);
       password=findViewById(R.id.passwordInscription_text);
        tel=findViewById(R.id.tel_text);

      confirmPassword =findViewById(R.id.confirmPasswordInscription_text);
      btnInscription=findViewById(R.id.inscription_button);


      btnInscription.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {


              IUser IUser = ApiClient.getClient().create(com.example.miniprojet.dao.IUser.class);
              IUser.executeSignup(nom.getText().toString(),prenom.getText().toString(),
                      email.getText().toString(),password.getText().toString(),tel.getText().toString()).enqueue(new Callback<User>() {
                  @Override
                  public void onResponse(Call<User> call, Response<User> response) {
                      Toast.makeText(getApplicationContext(), "succeed", Toast.LENGTH_LONG).show();
                      Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();



                  }

                  @Override
                  public void onFailure(Call<User> call, Throwable t) {
                      Toast.makeText(getApplicationContext(), "Veuillez vérifier votre boite de réception pour confirmer l'adresse", Toast.LENGTH_LONG).show();
                      Log.v("tag", "tag" + t.getMessage());

                  }
              });
          }
      });

          }





}
