package com.example.miniprojet;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.miniprojet.classes.ApiClient;
import com.example.miniprojet.dao.IUser;
import com.example.miniprojet.models.User;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements GoogleSignInApi {

    EditText  email,password ;
    Button btnLogin,btn,btnInscri;
    TextView signup ;

    public static final String FILE_NAME = "com.example.miniprojet.shared";
    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginpage);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        init();
        sharedPreferences = getSharedPreferences(FILE_NAME, MODE_PRIVATE);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        final GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent,1);
            }
        });



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                IUser IUser = ApiClient.getClient().create(IUser.class);
               IUser.loginn(email.getText().toString(),password.getText().toString()).enqueue(new Callback<User>() {
                   @Override
                   public void onResponse(Call<User> call, Response<User> response) {
                       Log.v("hhh", "hhh" + response.body().getId());
                       Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast. LENGTH_SHORT).show();
                      if(response.body().getMessage().equals("succed")){
                       SharedPreferences.Editor editor = sharedPreferences.edit();
                       editor.putString("LOGIN", email.getText().toString());
                       editor.putString("PASSWORD", password.getText().toString());
                       editor.apply();
                       Intent intent = new Intent(LoginActivity.this, AccueilPage.class);
                       startActivity(intent);
                       finish();}

                   }

                   @Override
                   public void onFailure(Call<User> call, Throwable t) {
                       Log.v("tag!!!!!!!", "tag" + t.getMessage());

                   }
               });
            }
        });


        if (sharedPreferences.contains("LOGIN")) {
            Intent intent = new Intent(getApplicationContext(), AccueilPage.class);
            startActivity(intent);
        }





        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, InscriptionActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == 1) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
           // updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TAG", "signInResult:failed code=" + e.getStatusCode());
           // updateUI(null);
        }
    }

    public void init()
    {
        email = findViewById(R.id.email_text);
        password = findViewById(R.id.password_text);
        btnLogin = findViewById(R.id.login_button);
       btn = findViewById(R.id.btn);
        signup=findViewById(R.id.signup);


    }




    @Override
    public Intent getSignInIntent(GoogleApiClient googleApiClient) {
        return null;
    }

    @Override
    public OptionalPendingResult<GoogleSignInResult> silentSignIn(GoogleApiClient googleApiClient) {
        return null;
    }

    @Override
    public PendingResult<Status> signOut(GoogleApiClient googleApiClient) {
        return null;
    }

    @Override
    public PendingResult<Status> revokeAccess(GoogleApiClient googleApiClient) {
        return null;
    }

    @Nullable
    @Override
    public GoogleSignInResult getSignInResultFromIntent(Intent intent) {
        return null;
    }
}
